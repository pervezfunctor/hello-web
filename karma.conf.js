module.exports = function(config) {
    config.set({
        basePath: '',
        frameworks: ['jasmine'],
        files: [ './spec/**/*-spec.js' ],
        exclude: [ './spec/**/flycheck*.js' ],

        reporters: ['dots'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: ['Chrome'],
        singleRun: false,

        webpackMiddleware: { noInfo: true },
        preprocessors: { './spec/**/*-spec.js': ['webpack'] },
        webpack: {
            module: {
                loaders: [
                    { test: /\.js$/, loader: 'babel' },
                    { test: /\.coffee$/, loader: 'coffee-loader' }
                ]
            },
        }
    });
};
