import curry from '../src/fp.coffee';

let add = function(x, y) {
    return x + y;
};

describe('fp', function() {
    it('curry', function() {
        //let inc = curry(add)(1);
        expect(add(1, 100)).toBe(101);
    });
});
