ncurry = (n, f, as = []) ->
  (bs...) ->
    cs = as.concat bs
    if cs.length < n then ncurry n, f, cs else f cs...

curry = (f, as=[]) -> ncurry f.length, f, as

module.exports = curry
